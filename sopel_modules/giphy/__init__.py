# coding=utf8
"""Sopel Giphy

Karma Sopel module
"""
from __future__ import unicode_literals, absolute_import, division, print_function

from .giphy import *

__author__ = 'W. Barbe'
__email__ = 'barbewaldemar@googlemail.com'
__version__ = '0.0.1'

