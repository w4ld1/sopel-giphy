# coding=utf8
"""
giphy.py

By Waldemar Barbe

"""
from __future__ import unicode_literals
from sopel.module import commands
import giphypop

giphy = giphypop.Giphy()

@commands('giphy')
# @example('.giphy nick')
def giphy(bot, trigger):
    nick = trigger.nick
    if trigger.group(2):
        url = giphy.random_gif(trigger.group(2)).media_url
    else:
        url = giphy.random_gif().media_url

    bot.say("%s" % (url))